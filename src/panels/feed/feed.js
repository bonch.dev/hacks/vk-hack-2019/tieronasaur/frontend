import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';

import './feed.sass'

import headerLogo from '../../img/LOGO.png'
import testObg from '../../img/testObj.png'

const Feed = ({go, selectedTags}) => {
    const [category, setCategory] = useState('new');
    const ChangeCategory = (category) => {
        setCategory(category)
    };
    return <div>
        <div className="defaultHeader">
            <div className="headerLogo">
                <img src={headerLogo}/>
            </div>
        </div>
        <div className="layoutSelector">
            {[{id: "new", text: "Новое"}, {id: "popular", text: "Популярное"}, {id: "friends", text: "Друзья"}].map(c => {
                return <button onClick={() =>ChangeCategory(c.id) } className={category === c.id?"layoutButtonSelected": ""}>{c.text}</button>
            })}
            <div className="filterButtons">
                {selectedTags.length > 0? <button data-to="tagsSelector" onClick={() => go("tagsSelector")} className="filterButton">
                    X
                </button> : null}

                <button data-to="tagsSelector" onClick={() => go("tagsSelector")} className="filterButton">
                    F
                    {selectedTags.length > 0? <div className="tagsCounter">{selectedTags.length}</div> : null}
                </button>
            </div>
        </div>
        <FeedList go={go}/>
    </div>

}


function FeedList({go}) {
    return <div className="feedList">
		{[1,2,3,4].map(a => {
			return <TierListPost id={a} go={go}/>
		})}
    </div>
}

function TierListPost(props) {
	return <div className="tierListPost" onClick={() => props.go('tierList')}>
		<span className="tierListHeader">Лучший мем с котом {props.id}</span>
		<span className="tierListUploadInfo">Артем Артемович 10 минут назад</span>
		<TierList/>
	</div>
}

function TierList() {
	return <div className="tierList">
		{[5,4,3,2,1].map(a => {
			return <div className="tierListRow">
				<div className="tierListRowObject" style={{backgroundColor: getColorOfTierList(a)}}>{a}</div>
				<div className="tierListRowObject">
                    <img src={testObg} alt=""/>
				</div>
			</div>
		})}
        <div className="bottomButtons">
            <div className="carma"></div>
            <div className="share"></div>
            <div className="views"></div>
        </div>
	</div>
}

Feed.propTypes = {
    // id: PropTypes.string.isRequired,
    go: PropTypes.func.isRequired,
    fetchedUser: PropTypes.shape({
        photo_200: PropTypes.string,
        first_name: PropTypes.string,
        last_name: PropTypes.string,
        city: PropTypes.shape({
            title: PropTypes.string,
        }),
    }),
};

export default Feed;

//miscs

function getColorOfTierList(a) {
    switch (a) {
        case 5:
            return "#82F167"
        case 4:
            return "#C5F737"
        case 3:
            return "#EEF145"
        case 2:
            return "#F1C145"
        case 1:
            return "#F16E45"
    }
}