import React, { useState, useEffect } from 'react';
import connect from '@vkontakte/vk-connect';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import './vkui.css';
import './antd.css'

import {Epic, Tabbar, TabbarItem, Panel, PanelHeader} from "@vkontakte/vkui";
import './main.sass'

import Feed from "./panels/feed/feed"
import TagsSelector from "./panels/feed/tagsSelector"
import TierList from "./panels/tierlist/tierlist"

const App = () => {
	const [activePanel, setActivePanel] = useState('tierList');
	const [activeStory, setActiveStory] = useState('feed');
	const [fetchedUser, setUser] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
	const [selectedTags, setSelectedTags] = useState(["Игры"]);

	const selectTag = (tag) => {
		if (selectedTags.includes(tag)) {
			setSelectedTags(selectedTags.filter((f => f !== tag)))
		} else {
			setSelectedTags(prevState => [...prevState, tag])
		}
	};

	const go = panel => {
		setActivePanel(panel);
	};

	const onStoryChange = e => {
		setActiveStory(e.currentTarget.dataset.story)
	}

	return (
		<Epic activeStory={activeStory} tabbar={
			<Tabbar>
				<TabbarItem
					onClick={() => onStoryChange}
					selected={activeStory === 'feed'}
					data-story="feed"
					text={activePanel}>
				</TabbarItem>
			</Tabbar>
		}>
			<View className="feed" id="feed" activePanel={activePanel}>
				<Panel id="feed">
					<Feed selectedTags={selectedTags} go={go}/>
				</Panel>
                <Panel id="tagsSelector">
                    <TagsSelector go={go} selectedTags={selectedTags} selectTag={selectTag}/>
                </Panel>
				<Panel id="tierList">
                    <TierList go={go} selectedTags={selectedTags} selectTag={selectTag}/>
                </Panel>
			</View>
		</Epic>)
};

export default App;
