import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';

import './feed.sass'

import headerLogo from '../../img/LOGO.png'
import testObg from '../../img/testObj.png'

import {Input} from 'antd'
const { Search } = Input;

const TagsSelector = ({go, selectedTags, selectTag}) => {
    return <div>
        <div className="defaultHeader">
            <div className="headerLogo">
                <img src={headerLogo}/>
            </div>
        </div>
        <div className="layoutSelector" style={{backgroundColor: "#501B94"}}>
            <input className="searchInput" placeholder="Поиск по тегам"/>
            <div className="backButton" onClick={() => go("feed")}>back</div>
        </div>
        <div className="tagsCloud">
            {["Игры","Программирование","Музыка","Мультфильмы","Наруто"].map(tag => {
                return <button onClick={() => selectTag(tag)} className={selectedTags.includes(tag)? "selected" : ""}>{tag}</button>
            })}
        </div>
        </div>
};

TagsSelector.propTypes = {
    // id: PropTypes.string.isRequired,
    go: PropTypes.func.isRequired,
    fetchedUser: PropTypes.shape({
        photo_200: PropTypes.string,
        first_name: PropTypes.string,
        last_name: PropTypes.string,
        city: PropTypes.shape({
            title: PropTypes.string,
        }),
    }),
};

export default TagsSelector;
