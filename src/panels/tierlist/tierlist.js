import React, { useState, useEffect } from 'react';

import './tierlist.sass'
import PropTypes from "prop-types";
import TagsSelector from "../feed/tagsSelector";
import headerLogo from "../../img/LOGO.png";

import {PanelHeader, HeaderButton, HorizontalScroll} from "@vkontakte/vkui";
import testObg from "../../img/testObj.png";

const TierList = (props) => {
    const [category, setCategory] = useState('all');
    const ChangeCategory = (category) => {
        setCategory(category)
    };

    return <div className="tierList">
        <div className="defaultHeader">
            <PanelHeader left={<HeaderButton onClick={() => props.go('feed')}>back</HeaderButton>}>Тирлист</PanelHeader>
        </div>
        <div className="layoutSelector">
            {[{id: "all", text: "Общее"}, {id: "author", text: "Автор"}, {id: "my_answer", text: "Мой ответ"}].map(c => {
                return <button onClick={() =>ChangeCategory(c.id) } className={category === c.id?"layoutButtonSelected": ""}>{c.text}</button>
            })}
        </div>
        <div className="tierListPost" onClick={() => props.go('tierList')}>
            <span className="tierListHeader">Лучший мем с котом {props.id}</span>
            <span className="tierListUploadInfo">Артем Артемович 10 минут назад</span>
            <div className="tierList">
                {[5,4,3,2,1].map(a => {
                    return <div className="tierListRow">
                        <div className="tierListRowObject" style={{backgroundColor: getColorOfTierList(a)}}>{a}</div>
                        <div className="tierListRowObject">
                            <img src={testObg} alt=""/>
                        </div>
                    </div>
                })}
                <div className="bottomButtons">
                    <div className="carma"></div>
                    <div className="share"></div>
                    <div className="views"></div>
                </div>
                <div className="tagsCloud">
                    {["Игры","Программирование","Музыка","Мультфильмы","Наруто"].map(tag => {
                        return <button>{tag}</button>
                    })}
                </div>
                <div className="friendsAnswers">
                    <span className="tierListHeader">Ответы друзей:</span>
                    {/*TODO: HorizontalScroll*/}
                    <HorizontalScroll><div style={{display: "flex"}}>
                        {[1,2,3,4,5,6,7].map(() => {
                            return <div className="answerBlock"></div>
                        })}
                    </div></HorizontalScroll>

                </div>

            </div>
        </div>
    </div>
}

TagsSelector.propTypes = {
    // id: PropTypes.string.isRequired,
    go: PropTypes.func.isRequired,
    fetchedUser: PropTypes.shape({
        photo_200: PropTypes.string,
        first_name: PropTypes.string,
        last_name: PropTypes.string,
        city: PropTypes.shape({
            title: PropTypes.string,
        }),
    }),
};

export default TierList

//miscs

function getColorOfTierList(a) {
    switch (a) {
        case 5:
            return "#82F167"
        case 4:
            return "#C5F737"
        case 3:
            return "#EEF145"
        case 2:
            return "#F1C145"
        case 1:
            return "#F16E45"
    }
}